module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    less: {
      development: {
        options: {
          paths: ["src/css"]
        },
        files: {
          "src/css/portfolio.css": "src/css/main.less"
        }
      }
    },
    concat: {
      js : {
        src : [
          'src/js/*'
        ],
        dest : 'dist/js/portfolio.min.js'
      }
    },
    cssmin : {
      css:{
        src: 'src/css/portfolio.css',
        dest: 'dist/css/portfolio.css'
      }
    },
    uglify : {
      js: {
        files: {
          'dist/js/portfolio.min.js' : [ 'dist/js/portfolio.min.js' ]
        }
      }
    },
    watch: {
      css: {
        files: ['src/css/main.less'],
        tasks: ['less:development', 'cssmin:css']
      },
      js: {
          files: ['src/js/*'],
          tasks: ['concat:js', 'uglify:js']
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.registerTask('default',
    [
      'less:development',
      'cssmin:css',
      'concat:js',
      'uglify:js'
    ]
  );
};