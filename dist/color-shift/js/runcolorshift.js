$(document).ready(function(){

	var $text = $('#text')
	var origTextColor = null;
	$text.mouseover(function(){
		origTextColor = $(this).css('color');
		$(this).shiftColor('#ff0000', 'color', 1000);
	});
	$text.mouseout(function(){
		$(this).shiftColor(origTextColor, 'color', 1000);
	});

	var $backgroundDiv = $('#background');
	var origBgColor = null;
	$backgroundDiv.mouseover(function(){
		origBgColor = $(this).css('background-color');
		$(this).shiftColor('darkgreen', 'background-color', 1000);
		$(this).shiftColor('white', 'color', 1000);
	});
	$backgroundDiv.mouseout(function(){
		$(this).shiftColor(origBgColor, 'background-color', 1000);
		$(this).shiftColor('black', 'color', 1000);
	});

	var $borderDiv = $('#border')
	var origBorderColor = null;
	$borderDiv.mouseover(function(){
		origBorderColor = $(this).css('border-color');
		$(this).shiftColor('rgb(255,0,0)', 'border-color', 1000);
	});
	$borderDiv.mouseout(function(){
		$(this).shiftColor(origBorderColor, 'border-color', 1000);
	});


})