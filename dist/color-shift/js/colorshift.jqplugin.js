(function($){
    var $test = null;
    var running = false;
    $.fn.shiftColor = function(targetColor, attribute, time) {
        if(!running) {
            running = true;
            var randomID = getRandomID();
            $('body').append('<div id="' + randomID + '"></div>')
            $test = $('#' + randomID);
            var origColor = getColorByRGB(this, attribute);
            var targetColor = getColorByString(targetColor, attribute);
            var delta = getMaxDelta(origColor, targetColor);
            iterateColors(this, attribute, origColor, targetColor, delta, time);
            $test.remove();
            running = false;
        }
        return this;
    }

    function getColorByRGB(elem, attribute) {
        var colorString = elem.css(attribute);
        var matchColors = /rgb\((\d{1,3}), (\d{1,3}), (\d{1,3})\)/;
        var colorArray = matchColors.exec(colorString);
        return {
            red: parseInt(colorArray[1]),
            green: parseInt(colorArray[2]),
            blue: parseInt(colorArray[3])
        };
    }

    function getColorByString(color, attribute) {
        var options = {};
        options[attribute] = color;
        $test.css(options);
        return getColorByRGB($test, attribute);
    }

    function getMaxDelta(orig, target) {
        var max = 0;
        $.each(orig, function(key, val) {
            var delta = Math.abs(target[key] - val);
            if(delta > max) {
                max = delta;
            }
        });
        return max;
    }

    function iterateColors(elem, attribute, origColor, targetColor, delta, time) {
        if(time >= delta) {
            var delay = Math.floor(parseFloat(time) / parseFloat(delta)) - 1;
            var increment = 1;
        }
        else {
            var delay = 1;
            var increment = parseInt(delta) / parseInt(time);
            increment = Math.ceil(increment);
        }
        var interval = window.setInterval(function(){
            origColor.red = setColor(origColor.red, targetColor.red, increment);
            origColor.green = setColor(origColor.green, targetColor.green, increment);
            origColor.blue = setColor(origColor.blue, targetColor.blue, increment);

            var color = "rgb(" + origColor.red + "," + origColor.green + "," + origColor.blue + ")";
            var options = {};
            options[attribute] = color;
            elem.css(options);

            if(origColor.red == targetColor.red && origColor.green == targetColor.green && origColor.blue == targetColor.blue) {
                window.clearInterval(interval);
            }
        }, delay);
    }

    function setColor(orig, target, increment) {
        if(orig > target){
            orig -= increment;
        }
        else if(orig < target) {
            orig += increment;
        }
        if(Math.abs(orig - target) < increment){
            orig = target
        }
        return orig;
    }

    function getRandomID()
    {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for( var i=0; i < 32; i++ )
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        return text;
    }


})(jQuery);

