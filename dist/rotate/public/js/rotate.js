var Rotate = {
  $win: null,
  $body: null,
  $content: null,
  contentCenter: null,
  mouseLoc: null,
  $mouseX: null,
  $mouseY: null,
  $debugAngle: null,
  $reset: null,
  resetInterval: null,
  resetIterations: 360,
  previousAngleX: 0,
  previousAngleY: 0,
  previousAngleZ: 0,
  adjustedAngleX: 0,
  adjustedAngleY: 0,
  adjustedAngleZ: 0,
  offset: 0,
  angle: 0,
  debug: false,

  init: function() {
    var rotate = this;
    // comment out the line below to remove debug text
    this.debug = true;
    this.$win = $(window);
    this.$body = $('body');
    this.$content = $('#content');
    this.$mouseX = $('#mouse_x');
    this.$mouseY = $('#mouse_y');
    this.$debugAngle = $('#angle');
    this.$reset = $('#reset');
    // prevent the image to be rotated from being "dragged"
    this.$content.mousedown(function(event){
      event.preventDefault();
    })
    // get center of image onload and when window is resized.
    this.contentCenter = this.getContentCenter();
    this.$win.resize(function() {
      rotate.contentCenter = rotate.getContentCenter();
    });
    rotate.initMouseListeners();

    $('.no-transform-popup .close').click(function(){
      $('.no-transform-popup').addClass('hidden');
    })
  },

  getContentCenter: function() {
    var center = {};
    center.x =  parseInt(this.$content.offset().left) + parseInt(this.$content.width()) / 2;
    center.y = parseInt(this.$content.offset().top) + parseInt(this.$content.height()) / 2;
    if(this.debug) {
      $('#content_center_x').text('x ' + center.x);
      $('#content_center_y').text('y ' + center.y);
      $('#center-div').css({
        left: center.x,
        top: center.y
      });
      $('.debug').each(function(){
        $(this).addClass('active');
      });
    }
    return center;
  },

  initMouseListeners: function() {
    var rotate = this;
    this.$body.mousedown(function(event){
      rotate.offset = rotate.getAngle(event.pageX, event.pageY);
      $(this).mousemove(function(event) {
        rotate.onMouseMove(event);
      });
    });
    this.$body.mouseup(function(){
      rotate.previousAngleX = rotate.adjustedAngleX;
      rotate.previousAngleY = rotate.adjustedAngleY;
      rotate.previousAngleZ = rotate.adjustedAngleZ;
      $(this).off('mousemove');
    });
    $(document).mouseout(function(){
      $(this).off('mousemove');
    });

    this.$reset.click(function(){
      rotate.resetRotation();
    })
  },

  onMouseMove: function(event) {
    this.mouseLoc = {
      x: event.pageX,
      y: event.pageY
    };
    if(this.debug) {
      this.$mouseX.text('x ' + this.mouseLoc.x);
      this.$mouseY.text('y ' + this.mouseLoc.y);
    }
    this.angle = this.getAngle(this.mouseLoc.x, this.mouseLoc.y);
    this.rotate();
  },

  getAngle: function(x,y) {
    var quadrant = this.getQuadrant(x, y);
    switch (quadrant) {
      case 1:
        var dx = this.contentCenter.x - x;
        var dy = this.contentCenter.y - y;
        var angleRad = Math.atan2(dy,dx);
        var angleDeg = (angleRad * 180/Math.PI) - 90;
        break;
      case 2:
        var dx = this.contentCenter.x - x;
        var dy = this.contentCenter.y - y;
        var angleRad = Math.atan2(dy,dx);
        var angleDeg = (angleRad * 180/Math.PI) + 270;
        break;
      case 3:
        var dx = this.contentCenter.x - x;
        var dy = this.contentCenter.y - y;
        var angleRad = Math.atan2(dy,dx);
        var angleDeg = (angleRad * 180/Math.PI) + 270;
        break;
      case 4:
        var dx = x - this.contentCenter.x;
        var dy = y - this.contentCenter.y;
        var angleRad = Math.atan2(dy,dx);
        var angleDeg = (angleRad * 180/Math.PI) + 450;
        break;
    }
    // handle due n,s,e,w
    if(x == this.contentCenter.x) {
      if(y > this.contentCenter.y) {
        angleDeg = 180;
      }
      else {
        angleDeg = 0;
      }
    }
    if(y == this.contentCenter.y) {
      if(x > this.contentCenter.x) {
        angleDeg = 90;
      }
      else {
        angleDeg = 270;
      }
    }

    if(this.debug) {
      this.$debugAngle.text(angleDeg + ' degrees');
    }
    return angleDeg;
  },

  getQuadrant: function(x,y) {
    var quad = 0;
    // right
    if(x > this.contentCenter.x) {
      // above (quad 1)
      if(y < this.contentCenter.y) {
        quad = 1;
      }
      // below (quad 2)
      else {
        quad = 2;
      }
    }
    // left
    else {
      // below (quad 3)
      if(y > this.contentCenter.y) {
        quad = 3;
      }
      // above (quad 4)
      else {
        quad = 4;
      }
    }
    return quad; 
  },

  rotate: function() {
    var rotate = this;
    var val = $('input[name=axis]:checked').val();
    switch(val) {
      case 'x':
        rotate.rotateX();
        break;
      case 'y':
        rotate.rotateY();
        break;
      case 'z':
        rotate.rotateZ();
        break;
    };
    rotate.rotateContent();
  },

  rotateX: function() {
    var rotate = this;
    rotate.adjustedAngleX = (rotate.previousAngleX + rotate.angle - rotate.offset) % 360;
  },

  rotateY: function() {
    var rotate = this;
    rotate.adjustedAngleY = (rotate.previousAngleY + rotate.angle - rotate.offset) % 360;
  },

  rotateZ: function() {
    var rotate = this;
    rotate.adjustedAngleZ = (rotate.previousAngleZ + rotate.angle - rotate.offset) % 360;
  },

  rotateContent: function() {
    var rotate = this;
    this.$content.css({
      '-webkit-transform': 'rotateX(' + rotate.adjustedAngleX + 'deg) rotateY(' + rotate.adjustedAngleY + 'deg) rotateZ(' + rotate.adjustedAngleZ + 'deg)',
      '-moz-transform': 'rotateX(' + rotate.adjustedAngleX + 'deg) rotateY(' + rotate.adjustedAngleY + 'deg) rotateZ(' + rotate.adjustedAngleZ + 'deg)',
      '-ms-transform': 'rotateX(' + rotate.adjustedAngleX + 'deg) rotateY(' + rotate.adjustedAngleY + 'deg) rotateZ(' + rotate.adjustedAngleZ + 'deg)'
    });
  },

  resetRotation: function() {
    var rotate = this;
    var i = 0;
    var adjustmentX = this.getAdjustment(this.adjustedAngleX);
    var adjustmentY = this.getAdjustment(this.adjustedAngleY);
    var adjustmentZ = this.getAdjustment(this.adjustedAngleZ);
    console.log('x ' + rotate.adjustedAngleX);
      console.log('y ' + rotate.adjustedAngleY);
      console.log('z ' + rotate.adjustedAngleZ);
      console.log('adj x ' + adjustmentX);
      console.log('adj y ' + adjustmentY);
      console.log('adj z ' + adjustmentZ);
      console.log('------------------------------------------')

    rotate.resetInterval = setInterval(function(){
      rotate.adjustedAngleX = rotate.resetAxis(rotate.adjustedAngleX, adjustmentX);
      rotate.adjustedAngleY = rotate.resetAxis(rotate.adjustedAngleY, adjustmentY);
      rotate.adjustedAngleZ = rotate.resetAxis(rotate.adjustedAngleZ, adjustmentZ);
      console.log('x ' + rotate.adjustedAngleX);
      console.log('y ' + rotate.adjustedAngleY);
      console.log('z ' + rotate.adjustedAngleZ);
      rotate.rotateContent();
      if(!(rotate.adjustedAngleX + rotate.adjustedAngleY + rotate.adjustedAngleZ)) {
        clearInterval(rotate.resetInterval);
        rotate.previousAngleX = 0;
        rotate.previousAngleY = 0;
        rotate.previousAngleZ = 0;
      }
    }, 15)
  },

  getAdjustment: function(angle) {
    return Math.abs(this.resetIterations / (360 - Math.abs(angle)));
  },

  resetAxis: function(angle, adjustment) {
    var newAngle = 0;
    if(angle > 0){
      if(angle < 180) {
        newAngle = angle - adjustment;
        if(Math.abs(angle) < adjustment) {
          newAngle = 0;
        }
      }
      else {
        newAngle = (angle + adjustment) % 360;
        if(360 - Math.abs(angle) < adjustment) {
          newAngle = 0;
        }
      }
    }
    else {
      if(angle > -180) {
        newAngle = angle + adjustment;
        if(Math.abs(angle) < adjustment) {
          newAngle = 0;
        }
      }
      else {
        newAngle = (angle - adjustment) % 360;
        if(360 - Math.abs(angle) < adjustment) {
          newAngle = 0;
        }
      }
    }
    return newAngle;
  }

};