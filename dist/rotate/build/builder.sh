#!/bin/bash
# Number of expected arguments
EXPECTED_ARGS=2

# Make sure the folder in the argument exists
function check_folder_existence {
  if [ -d $1 ]; then
    echo ""
  else
    echo "Target build path does not exist."
    exit
  fi
}

# Check for file existence and then cat all files together to one
function check_cat_file_existence {
  if [ -f $2 ]; then
    # Replace the special characters with normal / characters
    tr '\\' '/' < $2 >> tmp_1
    tr '\r' '\n' < tmp_1 >> tmp_2

    (cat fileList | tail -n +2 tmp_2) > fileList

    cat $(grep -v '^#' fileList) > test

    mv test $1$(head -n +1 tmp_2)

    echo "Created $(head -n +1 tmp_2)"
    rm tmp_1 tmp_2 fileList
  else
    echo "The file(s) you selected do not exist."
    exit
  fi
}

function compress_cat {
  if [ -f $2 ]; then
    # Replace the special characters with normal / characters
    tr '\\' '/' < $2 >> tmp_1
    tr '\r' '\n' < tmp_1 >> tmp_2

    (cat fileList | tail -n +2 tmp_2) > fileList

    cat $(grep -v '^#' fileList) > _tmpjs.js

    java -jar ./build/yuicompressor-2.4.7.jar --disable-optimizations -o $1$(head -n +1 tmp_2) _tmpjs.js

    echo "Created $(head -n +1 tmp_2)"

    rm tmp_1 tmp_2 fileList _tmpjs.js
  else
    echo "The file(s) you selected do not exist."
    exit
  fi
}

if [ $# -ge $EXPECTED_ARGS ]; then
  # If no compress is the first argument
  if [ $1 = "--no-compress"  ] ; then
    check_folder_existence $2
    destination=$2
    shift
    shift
    for file; do
      check_cat_file_existence $destination $file
    done

  else  
    check_folder_existence $1
    destination=$1
    shift
    for file; do
      compress_cat $destination $file
    done

  fi
else
  echo "Inputed wrong number of arguments"
fi