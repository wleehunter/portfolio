var rotationComplete = false;
var clickLeverAngle = -30;
var leverAngle = -30;
var leverDragging = false;
var candyCaneDragging = false;
var dragLeverOn = false;
var labelsVisible = false;
var $candyCaneShuttle = null;
var candyCaneSliderBarWidth = 0;
var currentCandyCaneX = 0;
var craneKeysBound = false;
var keys = {};
var spinInterval = null;
var conveyorWheelAngle = 0;


$('document').ready(function(){

    // candy cane slider bar (controls phone)
    $candyCaneShuttle = $('#candy-cane-shuttle');
    candyCaneSliderBarWidth = $('#slider-bar').width();

    $candyCaneShuttle.mousedown(function(event){
        onCandyCaneMouseDownHandler(event);
    });


    // bind keyboard arrows to control crane
    $('#bind-crane-keys-button').live('click', function(event){
            event.preventDefault();
            $(document).keydown(function(event){
                handleCraneKeyPress(event);
            });
            $(this).hide();
            $('#unbind-crane-keys-button').show();
            $('#crane-stage').css({
                'backgroundPosition': '273px 0, 0 340px'
            });
            return false;
    });

    $('#unbind-crane-keys-button').live('click', function(event){
        event.preventDefault();
        $(document).unbind('keydown');
        $(this).hide();
        $('#bind-crane-keys-button').show();
    });


    // listener for lever
    $('#box-lever-drag').mousedown(function(event){
        onLeverMouseDownHandler(event);
    });

});

function explodePhone(value) {
    if(value > 0 && value <= 45){
        // #iphone-gumdrop, #iphone-candy-cane,
        $('#iphone-back, #iphone-front').css({
            '-webkit-transform': 'rotateY(' + value + 'deg)',
            '-moz-transform': 'rotateY(' + value + 'deg)',
            '-ms-transform': 'rotateY(' + value + 'deg)'
        });
    }
    else{
        var i = value - 45;
        $('#iphone-back').css('margin-left', (-1 * i) + 'px');
        $('#iphone-gumdrop').css('margin-left', ((-1 * i) * .4) + 'px');
        $('#iphone-candy-cane').css('margin-left', (i * .2) + 'px');
        $('#iphone-front').css('margin-left', i + 'px');
    }
    // Parts are sometimes are a couple pixels off when phone comes back together, so manually reset them.
    if(value == 0) {
        $('#iphone-back, #iphone-gumdrop, #iphone-candy-cane, #iphone-front').css('margin-left', '0px');
        $('#iphone-back, #iphone-front').css({
            '-webkit-transform': 'rotateY(0deg)',
            '-moz-transform': 'rotateY(0deg)',
            '-ms-transform': 'rotateY(0deg)'
        });
    }
    showLabels(value);
}

// Show the labels for the various parts
function showLabels(value){
    if(value > 190 && !labelsVisible && $('#gumdrop-label, #candy-cane-label').css('display') == 'none'){
        $('#gumdrop-label, #candy-cane-label')
            //.css('display', 'block');
            .fadeIn(800, function(){
                labelsVisible = true;
            });
        //labelsVisible = true;
    }
    else if(value <= 190 && labelsVisible && $('#gumdrop-label, #candy-cane-label').css('display') == 'block') {
        $('#gumdrop-label, #candy-cane-label')
            //.css('display', 'none');
            .fadeOut(200, function(){
                labelsVisible = false;
            })
        //labelsVisible = false;
    }
}


function onCandyCaneMouseDownHandler(downEvent){
    downEvent.preventDefault();
    //disables text selection of the body so that user doesn't highlight all of the steps when dragging
    $("body").disableSelection();

    candyCaneDragging = true;
    var startX = parseInt($candyCaneShuttle.css('marginLeft'));

    var onDrag = function(moveEvent){
        onCandyCaneMouseMoveHandler(moveEvent, downEvent, startX);
    };
    var onDragStop = function(upEvent){
        onCandyCaneMouseUpHandler(upEvent, downEvent);
    };

    $('#slider-area')
        .mousemove(onDrag)
        .mouseup(onDragStop)
        .mouseleave(onDragStop);

    $('body')
        .mouseup(onDragStop)
        .mouseleave(onDragStop);
}

function onCandyCaneMouseMoveHandler(moveEvent, downEvent, startX) {
    var maxWidth = candyCaneSliderBarWidth - $candyCaneShuttle.width();
    var dx = moveEvent.screenX - downEvent.screenX;
    var position = (dx + startX).bound(0, maxWidth);
    $candyCaneShuttle.css({'marginLeft': position});
    currentCandyCaneX = parseInt($candyCaneShuttle.css('marginLeft'));
    var percentage = parseInt((currentCandyCaneX / maxWidth) * 100);
    explodePhone(percentage * 2);

}

function onCandyCaneMouseUpHandler(upEvent, downEvent) {
    if(candyCaneDragging){
        candyCaneDragging = false;
        $('#slider-area')
            .unbind("mousemove")
            .unbind("mouseup")
            .unbind("mouseleave");
    }
}


function onLeverMouseDownHandler(downEvent){
    downEvent.preventDefault();
    //disables text selection of the body so that user doesn't highlight all of the steps when dragging
    $("body").disableSelection();

    leverDragging = true;
    var startX = parseInt(downEvent.screenX);

    var onDrag = function(moveEvent){
        onLeverMouseMoveHandler(moveEvent, downEvent, startX);
    };
    var onDragStop = function(upEvent){
        onLeverMouseUpHandler(upEvent, downEvent);
    };

    $('#box-lever-drag')
        .mousemove(onDrag)
        .mouseup(onDragStop)
        .mouseleave(onDragStop);

    $('body')
        .mouseup(onDragStop)
        .mouseleave(onDragStop);
}


function onLeverMouseMoveHandler(moveEvent, downEvent, startX) {
    var dx = moveEvent.screenX - startX;
    if(dragLeverOn) {
        dx += 30;
    }
    else {
        dx -= 30;
    }
    if(dx >= -30 && dx <= 30) {
        positionLever(dx);
    }
}

// This is where any actions that result from the lever should be called
function positionLever(dx) {
    $('#lever-drag').css({
        '-webkit-transform': 'rotateZ(' + dx + 'deg)',
        '-moz-transform': 'rotateZ(' + dx + 'deg)',
        '-ms-transform': 'rotateZ(' + dx + 'deg)'
    });
    leverAngle = dx;
    
    var adjusted = dx + 30;
    var percentage = parseInt((adjusted / 60) * 100);
    //explodePhone(percentage * 2);
}

function onLeverMouseUpHandler(upEvent, downEvent) {
    if(leverDragging){
        leverDragging = false;
        $('#box-lever-drag')
            .unbind("mousemove")
            .unbind("mouseup")
            .unbind("mouseleave");
        
        adjustLeverOnMouseUp(leverAngle);
    }

}

function adjustLeverOnMouseUp(angle) {
    var i = angle;
    if(i <= 0) {
        var rotationInterval = setInterval(function(){
            if(i > -30) {
                i--;
                positionLever(i);
            }
            else {
                window.clearInterval(rotationInterval);
            }
        },
        10);
        leverAngle = -30;
        dragLeverOn = false;
    }
    else {
        var rotationInterval = setInterval(function(){
            if(i < 30) {
                i++;
                positionLever(i);
            }
            else {
                window.clearInterval(rotationInterval);
            }
        },
        10);
        leverAngle = 30;
        dragLeverOn = true;
    }
    spinWheels(dragLeverOn);
}


function handleCraneKeyPress(event){
    if((event.keyCode > 36 && event.keyCode < 41) || event.keyCode == 13) {
        event.preventDefault();
            switch(event.keyCode) {
                case 13:
                    grabItem();
                    break;
                case 37:
                    var marginLeft = parseInt($('#crane').css('marginLeft'));
                    if(marginLeft > -30) {
                        marginLeft -= 10;
                        $('#crane').css('marginLeft', marginLeft);
                    }
                    break;
                case 38:
                    var marginTop = parseInt($('#crane').css('marginTop'));
                    if(marginTop > -282) {
                        marginTop -= 10;
                        $('#crane').css('marginTop', marginTop);
                    }
                    break;
                case 39:
                    var marginLeft = parseInt($('#crane').css('marginLeft'));
                    if(marginLeft < 287) {
                        marginLeft +=10;
                        $('#crane').css('marginLeft', marginLeft);
                    }
                    break;
                    
                case 40:
                    var marginTop = parseInt($('#crane').css('marginTop'));
                    if(marginTop < -4) {
                        marginTop +=10;
                        $('#crane').css('marginTop', marginTop);
                    }
                    break;
            }
        return false;
    }
}

function grabItem(){
    var marginLeft = parseInt($('#crane').css('marginLeft'));
    var marginTop = parseInt($('#crane').css('marginTop'));
    var targetTop = -50;
    var targetLeft = 278;
    var topCorrect = false;
    var leftCorrect = false;
    if((marginLeft >= 220 && marginLeft < 300) && (marginTop > -180 && marginTop < 0)) {
        var positionToGrabInterval = setInterval(function(){
            // adjust top margin
            if(marginTop > targetTop) {
                marginTop--;
            }
            else if(marginTop < targetTop) {
                marginTop++;
            }
            else if(marginTop == targetTop) {
                topCorrect = true;
            }
            // adjust left margin
            if(marginLeft > targetLeft) {
                marginLeft--;
            }
            else if(marginLeft < targetLeft) {
                marginLeft++;
            }
            else if(marginLeft == targetLeft) {
                leftCorrect = true;
            }
            // move the crane into place to grab item
            $('#crane').css({
                'marginTop': marginTop,
                'marginLeft': marginLeft
            });
            // if it's in place then pick it up.
            if(topCorrect && leftCorrect) {
                window.clearInterval(positionToGrabInterval);
                liftItem();
            }
        },
        10);
    }
}

function adjustArray(array) {
    return [array[1], array[3], array[5], array[7]];
}

function liftItem() {
    var targetMaxHeight = -170;
    var craneMarginTop = parseInt($('#crane').css('marginTop'));
    var stageBackgroundPosition = $('#crane-stage').css('backgroundPosition');
    var backgroundArray = stageBackgroundPosition.split(' ');

    if(backgroundArray[0] == 'left') {
        backgroundArray = adjustArray(backgroundArray);
    }
    var itemMarginTop = parseInt(backgroundArray[1]);

    var liftInterval = setInterval(function(){
        if(craneMarginTop > targetMaxHeight) {
            craneMarginTop--;
            itemMarginTop--;
            var backgroundString = backgroundArray[0] + ' ' + itemMarginTop + 'px, ' + backgroundArray[2] + ' ' + backgroundArray[3];
            $('#crane').css('marginTop', craneMarginTop);
            $('#crane-stage').css({'backgroundPosition': backgroundString});
        }
        else {
            window.clearInterval(liftInterval);
            moveItemToHole();
        }
    }, 10);
}

function moveItemToHole() {
    var targetLeft = -20;
    var craneMarginLeft = parseInt($('#crane').css('marginLeft'));
    var stageBackgroundPosition = $('#crane-stage').css('backgroundPosition');
    var backgroundArray = stageBackgroundPosition.split(' ');
    if(backgroundArray[0] == 'left') {
        backgroundArray = adjustArray(backgroundArray);
    }
    var itemMarginLeft =parseInt(backgroundArray[0]);

    var moveInterval = setInterval(function(){
        if(craneMarginLeft > targetLeft) {
            craneMarginLeft--;
            itemMarginLeft--;
            var backgroundString = itemMarginLeft + 'px ' + backgroundArray[1] + ' ' + backgroundArray[2] + ' ' + backgroundArray[3];
            $('#crane').css('marginLeft', craneMarginLeft);
            $('#crane-stage').css({'backgroundPosition': backgroundString});
        }
        else {
            window.clearInterval(moveInterval);
            dropItem();
        }
    }, 10);

}

function dropItem() {
    var targetY = 125;
    var stageBackgroundPosition = $('#crane-stage').css('backgroundPosition');
    var backgroundArray = stageBackgroundPosition.split(' ');
    if(backgroundArray[0] == 'left') {
        backgroundArray = adjustArray(backgroundArray);
    }
    var itemMarginTop = parseInt(backgroundArray[1]);

    var dropInterval = setInterval(function(){
        if(itemMarginTop < targetY) {
            itemMarginTop += 15;
            var backgroundString = backgroundArray[0] + ' ' + itemMarginTop + 'px, ' + backgroundArray[2] + ' ' + backgroundArray[3];
            $('#crane-stage').css({'backgroundPosition': backgroundString});
        }
        else {
            window.clearInterval(dropInterval);
        }
    }, 10);
}

function spinWheels(on) {
    if(on) {
        clearInterval(spinInterval);
        spinInterval = setInterval(function(){
                $('.conveyor-wheel').css({
                    '-webkit-transform': 'rotateZ(' + conveyorWheelAngle + 'deg)',
                    '-moz-transform': 'rotateZ(' + conveyorWheelAngle + 'deg)',
                    '-ms-transform': 'rotateZ(' + conveyorWheelAngle + 'deg)'
                });
                conveyorWheelAngle+=2;
                conveyorWheelAngle % 360;
        }, 40);
        moveBeltLine(true);
    }
    else {
        clearInterval(spinInterval);
        moveBeltLine(false);
    }
}

function moveBeltLine(move){
    if(move){
        $('#belt-line')
            .css('marginLeft', '29px')
            .stop()
            .animate({
                'margin-left': 293
            },2500,function(){
                moveBeltLine(true);
            });
    }
    else {
        $('#belt-line')
            .css('marginLeft', '29px')
            .stop();
    }
}




(function($){
    $.fn.disableSelection = function() {
        return this
            .attr('unselectable', 'on')
            .css('user-select', 'none')
            .on('selectstart', false);
    };
})(jQuery);

Number.prototype.bound = function(low/*Number*/, high/*Number*/){
    if(!(isNaN(low) || isNaN(high)) && low > high){
        throw new Error("low cannot be greater than high");
    }
    if(high != undefined && this > high){ 
        return high;
    }
    if(low != undefined && this < low){
        return low;
    }
    return this.valueOf()/*Number*/;
};
