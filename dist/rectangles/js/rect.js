var Rect = {
  dragging: false,
  initX: 0,
  initY: 0,
  color: '#000',
  $container: null,
  $rect: null,
  enabled: false,

  init: function(elem) {
    var rect = this;
    rect.$container = $(elem);
    $('#no-box').click(function() {
      rect.handleNoBoxClick();
    });
    $('#clear-all').click(function(){
      rect.clearAll();
    });
    rect.$container.mousedown(function(event){
      rect.onMouseDownHandler(event);
    });
    $(document).on('click','a.close',function(event){
      event.preventDefault();
      rect.closeRect(this);
    }); 
  },

  onMouseDownHandler: function(downEvent) {
    console.log($(downEvent));
    var rect = this;
    if(rect.enabled && rect.isDrawableArea(downEvent)) {
      rect.$rect = $('<div>').addClass('rect');
      var close = $('<a>').attr({href: '#'}).addClass('close').html('x');
      $(close).appendTo(rect.$rect);
      rect.dragging = true;
      rect.initX = downEvent.pageX;
      rect.initY = downEvent.pageY;

      rect.$rect.css({
        top: rect.initY,
        left: rect.initX,
        width: 0,
        height: 0,
        border: '1px solid ' + rect.color
      });

      rect.$rect.appendTo(rect.$container);

      var onDrag = function(moveEvent){
        rect.onShuttleMouseMoveHandler(moveEvent, downEvent)
      };
      var onDragStop = function(upEvent){
        rect.onShuttleMouseUpHandler(upEvent, downEvent);
      };

      rect.$container
          .mousemove(onDrag)
          .mouseup(onDragStop)
          .mouseleave(onDragStop);

      $(window)
          .mouseup(onDragStop)
          .mouseleave(onDragStop);
    }
  },

  onShuttleMouseMoveHandler: function(moveEvent, downEvent) {
    var rect = this;
    if(rect.dragging) {
      var dX = Math.abs(moveEvent.pageX - rect.initX);
      var dY = Math.abs(moveEvent.pageY - rect.initY);
      var newX, newY;
     
      newX = (moveEvent.pageX < rect.initX) ? (rect.initX - dX) : rect.initX;
      newY = (moveEvent.pageY < rect.initY) ? (rect.initY - dY) : rect.initY;

      rect.$rect.css({
        width: dX,
        height: dY,
        top: newY,
        left: newX
      })
    }
  },

  onShuttleMouseUpHandler: function(upEvent, downEvent){
    var rect = this;
    rect.dragging = false;
  },

  setColor: function(color) {
    console.log(color);
    var rect = this;
    rect.color = color;
  },

  clearAll: function() {
    var rect = this;
    $('.rect').remove();
    // rect.disable();
  },

  disable: function() {
    var rect = this;
    rect.enabled = false;
    $('#no-box').removeClass('disable');
  },

  enable: function() {
    var rect = this;
    rect.enabled = true;
  },

  handleNoBoxClick: function() {
    var rect = this;
    $('#no-box').toggleClass('disable');
    if(rect.enabled) {
      rect.disable();
    }
    else {
      rect.enable();
    }
  },

  closeRect: function(rect) {
    $(rect).parent('.rect').remove();
  },

  isDrawableArea: function(downEvent) {
    return (($(downEvent.srcElement).attr('id') == 'container') || ($(downEvent.srcElement).attr('class') == 'rect'));
  }
}