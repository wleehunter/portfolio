var Layout = {
  init: function() {
    var layout = this;
    $('.resume-trigger').click(function(){
      layout.onResumeClick();
    });
    $('.mobile-popup .close').click(function(){
      layout.onMobileCloseClick();
    });
  },

  onResumeClick: function() {
    console.log('clicked');
    if('download' in document.createElement('a')) {
      $('.resume-popup').toggleClass('active');
    }
    else {
      window.location.href = "resume.html";
    }
  },

  onMobileCloseClick: function() {
    $('.mobile-popup').removeClass('active');
  }

};