(function(w){
  getDimensions();

  $(window).resize(function(){
    getDimensions();
  });

  $(window).on('orientationchange', function(){
    getDimensions();
  });
  
  function getDimensions() {
    var outerWidth = window.outerWidth;
    var outerHeight = window.outerHeight;
    var innerWidth = window.innerWidth;
    var innerHeight = window.innerHeight;
    var pixelRatio = window.devicePixelRatio;

    $('#outer-width-value').text(outerWidth + ' px');
    $('#outer-height-value').text(outerHeight + ' px');

    $('#inner-width-value').text(innerWidth + ' px');
    $('#inner-height-value').text(innerHeight + ' px');

    $('#pixel-ratio').text(pixelRatio);
  }
  
})(window);

