var CardFlip = {
	$item: null,
	$items: null,
	$main: null,
	$mainContainer: null,
	$card: null,
	$front: null,
	$back: null,
	$currentCard: null,
	$backTemplate: null,
	$backContent: null,
	$preload: null,
	itemStats: null,
	clicked: false,
	csstransforms3d: false,

	init: function(supportsTranforms) {
		var cardFlipHandler = this;
		this.csstransforms3d = supportsTranforms;

		this.$items = $('.work-items');

		// want to load this template as quick as possible, before all those jquery lookups are cached
		var html = $.tmpl($('#work-item-template'), WorkItems);
  	this.$items.append(html);
		
		// cache all these lookups so they only have to be done once
		this.$item = $('.work-item');
		this.$main = $('.main');
		this.$mainContainer = $(".main-container");
		this.$card = $('#card');
		this.$front = $('.front');
		this.$back = $('.back');
		this.$backTemplate = $('#card-back-template');
		this.$backContent = $('.card-back-content');
		this.$preload = $('.image-preload');
		this.itemStats = {};

		this.fadeSiblings();

		this.$items.on('mouseout', function(){
			$(this).children('.work-item').removeClass('inactive');
		})
		
		this.$item.on('click', function(event){
			event.preventDefault();

			cardFlipHandler.unFadeSiblings();
			
			cardFlipHandler.$currentCard = $(event.currentTarget);
			cardFlipHandler.setCardBack(cardFlipHandler.$currentCard);
			if(cardFlipHandler.csstransforms3d) {
				cardFlipHandler.scrollToCardTopAndFlip(cardFlipHandler.$mainContainer);
			}
			else {
				cardFlipHandler.scrollToCardFadeAndSwitch(cardFlipHandler.$mainContainer);
			}
		});

		this.$back.on('click', function(event){
			cardFlipHandler.fadeSiblings();
			if(cardFlipHandler.csstransforms3d) {
				cardFlipHandler.scrollToCardTopAndFlip(cardFlipHandler.$currentCard);
			}
			else {
				cardFlipHandler.scrollToCardFadeAndSwitch(cardFlipHandler.$currentCard);
			}
		});

		// load all of the full-size images (the backs of the cards) into a hidden div
		// so that they'll be cached for when their card is clicked.
		var images = $.tmpl($('#image-preload-template'), WorkItems);
  	cardFlipHandler.$preload.append(images);

	},

	fadeSiblings: function() {
		this.$item.on('mouseover', function(event){
			var $over = $(event.delegateTarget);
			$over.removeClass('inactive');
			$over.siblings().addClass('inactive');
		});
	},

	unFadeSiblings: function() {
		this.$item.off('mouseover');
		this.$item.each(function(){
			$(this).removeClass('inactive');
		})
	},

	// find the info that corresponds to the id of the clicked card and use it for the card back
	setCardBack: function($clicked) {
		var id = $clicked.attr('id');
		var idx = null;
		$.each(WorkItems, function(i, obj){
			if(obj.id == id) {
				idx = i;
			}
		});
		var html = $.tmpl(this.$backTemplate, WorkItems[idx]);
  	this.$backContent.empty().append(html);
	},

	scrollToCardTopAndFlip: function($elem) {
		var cardFlipHandler = this;
		var i = 0;
		if($elem == cardFlipHandler.$mainContainer) {
    	$('body, html').animate({
      	scrollTop: $elem.offset().top
	     }, 400, function(){
	     		if(i > 0) {
	     			cardFlipHandler.$card.toggleClass('flipped');
	     		}
	     		i++;
	     });
  	}
  	else {
  		cardFlipHandler.$card.toggleClass('flipped');
  		window.setTimeout(function(){
  			$('body, html').animate({
	      	scrollTop: $elem.offset().top
		    }, 400);
  		}, 1000);
  	}
	},

	// Not running the flip animation in ie at all. I've heard people have had some success in 10.
	scrollToCardFadeAndSwitch: function($elem) {
		var cardFlipHandler = this;
		var i = 0;
  	$('body, html').animate({
    	scrollTop: $elem.offset().top
     }, 400, function(){
     		if(i > 0) {
     			if(cardFlipHandler.$front.css('display') == 'block') {
	     			cardFlipHandler.$front.fadeOut(400, function(){
	     				cardFlipHandler.$back.fadeIn(400);
	     			});
	     		}
	     		else {
	     			cardFlipHandler.$back.fadeOut(400, function(){
	     				cardFlipHandler.$front.fadeIn(400);
	     			});
	     		}
     		}
     		i++;
     });
	}
};