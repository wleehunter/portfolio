var WorkItems = [
	{
		id: "smashbox",
		title: "Love Me by Smashbox",
		text: "This is a Ruby on Rails powered Facebook app that we built for Smashbox, an Estee Lauder brand. The objective was to allow users to submit a photo and some text which would be composited and automatically uploaded to the user's Facebook profile to be used as their cover photo. More importantly, the photo was also shown on a digital billboard on top of the Roosevelt Hotel in Hollywood during the week of the Oscars. Basically, it was a really awesome project. <br><b>Update:</b> Soooo, this project ended up getting a LOT of attention. It won a <a target='_blank' href='http://www.clioimage.com/winners/2014/digital_mobile/entry.cfm?entryid=801410562'>CLIO Image Award</a> and was a finalist in its category for the <a target='_blank' href='http://industry.shortyawards.com/category/6th_annual/facebook_campaign/qi/smashbox-love-me-digital-billboard-experience'>Shorty Awards</a>.",
		image_card: "smashbox-card.jpg",
		image_full: "smashbox-full.jpg",
		link_text: 'Watch the Love Me by Smashbox Case Study',
		link: "https://www.youtube.com/watch?v=s3gNE4coj4M"
	},
	{
		id: "vzw",
		title: "Verizon Wireless Phone Simulators",
		text: "This project is a tool for creating phone simulators that are used on the Verizon Wireless support site. The simulators are used to help people learn their way around their phones. A collection of xml files is used as their data source and a Backbone.js framework is used to provide functionality. The tool was coded in a way that allows people with little or no experience in software development to create simulators by simply entering information in the xml files and taking screenshots on the phone. We have recently been working on adapting this project to both a mobile experience as well as a touchscreen experience for use in teaching sessions in-store. The mobile version has already been rolled out for some devices. Check it out on your phone or tablet!",
		image_card: "vzw-card.jpg",
		image_full: "vzw-full.jpg",
		link_text: "View a simulator on the Verizon Wireless support site",
		link: "http://support.verizonwireless.com/simulator/Samsung/galaxys4_jb/index.php"
	},
	{
		id: "noelf",
		title: "No Elf Left Behind",
		text: "Every year the company that I work for, Modea, sends a Holiday card to the world. In 2012 that card was \"No Elf Left Behind.\" The basic idea: The toys that elves have traditionally made (rocking horses, wooden dolls, etc.) aren't going to cut it anymore. Kids today want electronics. So Santa needs a tool to teach his elves to make more modern toys.",
		image_card: "no-elf-card.jpg",
		image_full: "no-elf-full.jpg",
		link_text: "Learn how Santa's elves are being educated using No Elf Left Behind",
		link: "http://www.noelfleftbehind.com/"
	},
	{
		id: "whodot",
		title: "WhoDot",
		text: "who.modea.com, or \"WhoDot,\" is an internal site at Modea, the company that I work for. It is written using tools built on top of the Zend framework in PHP. The site serves as a tool for people to better get to know their co-workers. Users are categorized based on the team that they work on and they can enter a variety of information about themselves including their contact info, social media info and a \"mad-libs\" style profile. Users can also add \"badges\" to their profile to give their co-workers a better idea of their outside interests. They can click on the badges to see the other users who share that interest.",
		image_card: "whodot-card.jpg",
		image_full: "whodot-full.jpg",
		link_text: "Sorry, WhoDot is awesome but it's an internal site. Can't show it to you unless you start working at Modea",
		link: "http://www.modea.com/careers"
	},
	{
		id: "mizuno",
		title: "Mizuno USA",
		text: "When I was an intern at Modea I was given the opportunity to work on the Mizuno USA site. This was my first experience working on a site of this size. It was also my first experience working on a Drupal site. It was a very challenging project and I learned a great deal about working with and customizing a CMS.",
		image_card: "mizuno-card.jpg",
		image_full: "mizuno-full.jpg",
		link_text: "View the Mizuno USA site",
		link: "http://www.mizunousa.com/"
	},
	{
		id: "chiquita",
		title: "Chiquita Snack Finder",
		text: "This project was done for Chiquita Bites and Chiquita Snack Chips. It uses Google Maps and a third party data store to provide locations that carry the specific products searched for.",
		image_card: "chiquita-card.jpg",
		image_full: "chiquita-full.jpg",
		link_text: "Get you some healthy snacks on Facebook",
		link: "https://www.facebook.com/ChiquitaSnacking/app_471855312873621"
	},
	{
		id: "riunite",
		title: "Riunite Sweet Chill Chatter",
		text: "This is a Ruby on Rails project that we did for Riunite Sweet wines. This project gave me experience in loading content using AJAX requests. It also gave me some very valuable experience in optimizing assets to try to speed up page loads.",
		image_card: "riunite-card.jpg",
		image_full: "riunite-full.jpg",
		link_text: "Chill out on the Riunite Sweet site",
		link: "http://riunitesweet.com"
	},
	{
		id: "ntelos",
		title: "nTelos Wireless landing pages",
		text: "This page is one in a series of landing pages that we have created for nTelos Wireless. For the most part they are static html and css with a small amount of javascript. We have also created a Ruby on Rails project which generates these landing pages from a template based on information entered into a web form.",
		image_card: "ntelos-card.jpg",
		image_full: "ntelos-full.jpg",
		link_text: "View the nTelos Wirelsss site",
		link: "http://www.ntelos.com/lp/warehouse-sale"
	}
	
];